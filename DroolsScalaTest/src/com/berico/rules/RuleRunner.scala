package com.berico.rules

import org.drools.KnowledgeBase


import org.drools.KnowledgeBaseFactory
import org.drools.builder.KnowledgeBuilder
import org.drools.builder.KnowledgeBuilderError
import org.drools.builder.KnowledgeBuilderErrors
import org.drools.builder.KnowledgeBuilderFactory
import org.drools.builder.KnowledgeBuilderErrors
import org.drools.builder.ResourceType
import org.drools.logger.KnowledgeRuntimeLogger
import org.drools.logger.KnowledgeRuntimeLoggerFactory
import org.drools.io.ResourceFactory
import org.drools.runtime.StatefulKnowledgeSession
import com.berico.model.weather.Temperature

object RuleRunner {

	  def main(args : Array[String]) : Unit = {
		  println("Creating Knowledge Session")
		  
		  var ksession : StatefulKnowledgeSession = GetKnowledgeSession()
		  
		  println("Creating and insertng Temperature")
		  
		  val shouldBeTooHot = new Temperature(100)
		  
		  val shouldBeTooCold = new Temperature(20)
		  
		  ksession.insert(shouldBeTooHot)
		  ksession.insert(shouldBeTooCold)
		  
		  println("Firing all rules")
		  
		  ksession.fireAllRules()
	  }
		  
	  def GetKnowledgeSession() : StatefulKnowledgeSession = {
		  var kbuilder : KnowledgeBuilder  = KnowledgeBuilderFactory.newKnowledgeBuilder()
		  ////Code//Eclipse_Workspaces//Scala_Workspace//DroolsTest//src//main//resources//
		  kbuilder.add(ResourceFactory.newFileResource("WeatherRules.drl"), ResourceType.DRL)
		  var kbase : KnowledgeBase = KnowledgeBaseFactory.newKnowledgeBase()
		  kbase.addKnowledgePackages(kbuilder.getKnowledgePackages())
		  var ksession : StatefulKnowledgeSession = kbase.newStatefulKnowledgeSession()
		  //var logger : KnowledgeRuntimeLogger = KnowledgeRuntimeLoggerFactory.newConsoleLogger(ksession)
		  ksession
	  }	
}